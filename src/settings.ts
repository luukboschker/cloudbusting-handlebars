import { Resource } from '@cloudbusting/modelbuilder/lib/model';

export interface Settings {
    templateDir: string;
    defaultDir: string;
    partialsDir: string;
    outputDir: string;
    outputFileExt: string;
    preprocess?(_: Resource[]): void;
}
