import * as fs from 'fs-extra';
import * as handlebars from 'handlebars';
import * as path from 'path';

export class Partials {
  /**
   * Register all files in the specified directory (and its subdirectories)
   * as partials with Handlebars.
   *
   * @param dir the directory to search for partials
   */
  register(dir: string): void {
    if (fs.existsSync(dir)) {
      const partials = this.walk(dir);

      partials
        .forEach((file) => {
          const partialName = file.substring(dir.length + 1);

          const content = fs.readFileSync(file, 'utf8');
          handlebars.registerPartial(partialName, content);
        });
    }
  }

  private readonly walk = (dir: string): Array<string> => {
    const files = fs.readdirSync(dir);
    const result: string[] = [];

    files.forEach((file) => {
      const fullFilename = path.join(dir, file);
      // tslint:disable-next-line: newline-per-chained-call
      if (fs.statSync(fullFilename).isDirectory()) {
        result.concat(this.walk(fullFilename));
      } else {
        result.push(`${dir}/${file}`);
      }
    });

    return result;
  };
}
