import * as fs from 'fs-extra';

export class Output {
  private readonly outputDir: string;

  private readonly outputFileExt: string;

  constructor(outputDir: string, outputFileExt: string) {
    this.outputDir = outputDir;
    this.outputFileExt = outputFileExt;
  }

  /**
   * Remove all files with output file extensionfrom the output directory
   */
  clean(): void {
    if (!fs.pathExistsSync(this.outputDir)) {
      fs.mkdirSync(this.outputDir);
    }

    const files = fs
      .readdirSync(this.outputDir)
      .filter((file) => file.endsWith(this.outputFileExt));

    files.forEach((file) => {
      fs.removeSync(`${this.outputDir}/${file}`);
    });
  }

  /**
   * Copy files to the outputDir's root.
   *
   * @param dir name of the directory whose files to copy to the outputDir
   */
  copy(dir: string): void {
    if (fs.pathExistsSync(dir)) {
      fs.copySync(dir, this.outputDir);
    }
  }
}
