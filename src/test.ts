import { Generator } from '.';

const generator = new Generator({
  templateDir: './test/templates',
  partialsDir: './test/templates/partials',
  defaultDir: './test/templates/.default',
  outputDir: './test/output'
});

generator.run('./test/aws-demo.json');
