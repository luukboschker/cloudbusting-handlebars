import * as fs from 'fs-extra';
import * as Handlebars from 'handlebars';
import * as path from 'path';

export class Templates {
  private readonly templateDir: string;

  private readonly cache: any = {};

  constructor(templateDir: string) {
    this.templateDir = templateDir;
  }

  get(resourceType: string): any {
    if (!Object.prototype.hasOwnProperty.call(this.cache, resourceType)) {
      this.loadInCache(resourceType);
    }

    return this.cache[resourceType];
  }

  private loadInCache(resourceType: string): void {
    const filename = fs
      .readdirSync(this.templateDir)
      .find((file: string) => file.startsWith(resourceType));

    if (filename) {
      const file = path.join(this.templateDir, filename);
      const content = fs.readFileSync(file, 'utf8');
      this.cache[resourceType] = Handlebars.compile(content);
    } else {
      this.cache[resourceType] = undefined;
    }
  }
}
