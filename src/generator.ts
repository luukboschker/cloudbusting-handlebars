import { ModelBuilder, Resource, Infraplan } from '@cloudbusting/modelbuilder';
import * as fs from 'fs-extra';
import * as path from 'path';
import * as Handlebars from 'handlebars';
import { Output } from './output';
import { Partials } from './partials';
import { Settings } from './settings';
import { Templates } from './templates';

/**
 * The generator prepares the output directory, initialize Handlebars
 * and then loads an infraplan and processes each resource in it
 * with the Handlebars template matching its resource type.
 */
export class Generator {
  readonly settings: Settings;

  private readonly templates: Templates;

  constructor({
    templateDir = './templates',
    defaultDir = './default',
    partialsDir = './partials',
    outputDir = './output',
    outputFileExt = '.txt',
    preprocess
  }: Partial<Settings>) {
    this.settings = {
      templateDir,
      defaultDir,
      partialsDir,
      outputDir,
      outputFileExt: ((outputFileExt.startsWith('.') ? '' : '.') + outputFileExt),
      preprocess
    };

    this.templates = new Templates(this.settings.templateDir);
  }

  registerHelper(name: string, delegate: Handlebars.HelperDelegate): Generator {
    Handlebars.registerHelper(name, delegate);

    return this;
  }

  run(infraplanFile: string): void {
    if (!fs.existsSync(infraplanFile)) {
      throw new Error(`Infraplan ${infraplanFile} not found.`);
    }

    const output = new Output(this.settings.outputDir, this.settings.outputFileExt);
    output.clean();

    output.copy(this.settings.defaultDir);

    const partials = new Partials();
    partials.register(this.settings.partialsDir);

    const content = fs.readFileSync(infraplanFile, 'utf-8');
    const infraplan = JSON.parse(content) as Infraplan;

    const builder = new ModelBuilder(infraplan);
    const resources = builder.build();

    if (this.settings.preprocess) {
      this.settings.preprocess(resources);
    }

    resources.forEach((resource) => {
      this.process(resource);
    });
  }

  private process(resource: Resource): void {
    const template = this.templates.get(resource.resourceType);

    if (!template) {
      throw new Error(`No template found for ${resource.resourceType}`);
    }

    const content = template(resource);

    const filename = path.join(this.settings.outputDir, `${resource.resourceType}.${resource.name}${this.settings.outputFileExt}`);

    fs.writeFileSync(filename, content);
  }
}
